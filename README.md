# Kitroyale – PHP-FPM Docker images for kit-projects

PHP-FPM Docker images for Kitroyale projects.

## Building the images

    $ make build TAG=7.3-fpm

## Dockerfile environment variables

## Core variables

| Name             | Default value |
| :--------------- | :-----------: |
| KIT_PHP_PORT     |     9000      |
| KIT_NGINX_PORT   |     8000      |
| KIT_SERVER_NAME  |   localhost   |
| KIT_APP_ROOT     |   /kit-app    |
| KIT_WWW_ROOT     | /kit-app/www  |
| KIT_SSHD         |     false     |
| KIT_EFS_DNS_NAME |      ""       |

## PHP variables

| Name                         |      Default value       |
| :--------------------------- | :----------------------: |
| KIT_PHP_PING_PATH            |        /ping-fpm         |
| KIT_PHP_PING_PONG            | "Pong ok - kit-php:$TAG" |
| KIT_PHP_MEMORY_LIMIT         |            2G            |
| KIT_PHP_PM_TRUSTED_IPS       |        127.0.0.1         |
| KIT_PHP_PM_STATUS_PATH       |       /status-fpm        |
| KIT_PHP_PM                   |         dynamic          |
| KIT_PHP_PM_MAX_CHILDREN      |            10            |
| KIT_PHP_PM_START_SERVERS     |            4             |
| KIT_PHP_PM_MIN_SPARE_SERVERS |            2             |
| KIT_PHP_PM_MAX_SPARE_SERVERS |            6             |
| KIT_PHP_PM_MAX_REQUESTS      |            0             |
