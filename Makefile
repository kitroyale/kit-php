.PHONY: build

export REPO_NAME = kitroyale/kit-php
export TAG ?= 7.3-fpm
export KTAG ?= -rc24
export KIT_SSHD ?= true

shell = $(if $(findstring -alpine, $(TAG)),/bin/ash,/bin/bash)

#--no-cache
build:
	docker build --build-arg TAG --build-arg KTAG -t ${REPO_NAME}:${TAG}${KTAG} .

push:
	docker push ${REPO_NAME}:${TAG}${KTAG}

cli:
	docker run -e KIT_SSHD=${KIT_SSHD} -i -t -p 447:22 -p 9000:9000 ${REPO_NAME}:${TAG}${KTAG} ${shell}

run-php-tests: build
	docker run -i -t --rm -p 9000:9000 ${REPO_NAME}:${TAG}${KTAG} ${shell} -c \
	"docker-php-source extract && cd /usr/src/php; \
	TEST_PHP_EXECUTABLE=/usr/local/bin/php php run-tests.php -w ./failed-php-tests.txt -xq \
	&& docker-php-source delete"

compo-up:
	docker-compose up

compo-down:
	docker-compose down -v

compo-ls:
	docker-compose run phpfpm ls -al /kit-app

compo-build:
	docker-compose build --build-arg KIT_SSHD=${KIT_SSHD} --parallel --no-cache
	docker-compose up #--no-deps

ssh:
	ssh -o "StrictHostKeyChecking=no" root@localhost -p447

codebuild:
	$(if ${AWS_PROFILE},,${error AWS_PROFILE is not set})
	codebuild_build.sh -p ${AWS_PROFILE} -i ${CODEBUILD_IMAGE} -a . -m -c -b .buildspec.yml -e .buildspec.env
