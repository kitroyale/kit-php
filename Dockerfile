#
# Kitroyale - kit-php FPM base image for kit-projects.
#
ARG TAG=7.3-fpm
FROM php:$TAG
# FROM php:$TAG-alpine
ARG TAG
ARG KTAG=" base"
ENV KTAG $KTAG

LABEL vendor "com.kitroyale.php-fpm" maintainer="franck@kitroyale.com"

RUN apt-get update && apt-get install -y gettext-base lsof unzip

# ADD https://github.com/aws/efs-utils/archive/v1.25-1.tar.gz /tmp/efs-utils
# RUN cd /tmp/efs-utils ./build-deb.sh
# RUN apt-get -y install ./build/amazon-efs-utils*deb

# Install php extension helper
ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/
RUN chmod uga+x /usr/local/bin/install-php-extensions && sync

# Install some php extensions
RUN install-php-extensions apcu zip \
  bcmath \
  gd \
  intl \
  mcrypt \
  opcache \
  pdo_mysql \
  redis \
  soap \
  sockets \
  xsl

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- \
  --install-dir=/usr/local/bin --filename=composer \
    && composer global require hirak/prestissimo \
    && composer clear-cache

# Cleanup
RUN rm -rf /tmp/* /var/tmp/* /usr/share/doc/* && apt-get -y autoremove && apt-get -y clean

# Default env-vars
ENV KIT_PHP_PORT 9000
ENV KIT_NGINX_PORT 8000
ENV KIT_SERVER_NAME localhost
ENV KIT_APP_ROOT /kit-app
ENV KIT_WWW_ROOT /kit-app/pub
ENV KIT_PHP_MEMORY_LIMIT 2G
ENV KIT_PHP_PM dynamic
ENV KIT_PHP_PM_MAX_CHILDREN 10
ENV KIT_PHP_PM_START_SERVERS 4
ENV KIT_PHP_PM_MIN_SPARE_SERVERS 2
ENV KIT_PHP_PM_MAX_SPARE_SERVERS 6
ENV KIT_PHP_PM_MAX_REQUESTS 0
ENV KIT_PHP_PM_STATUS_PATH /status-fpm
ENV KIT_PHP_PM_TRUSTED_IPS 127.0.0.1
ARG KIT_SSHD=false
ENV KIT_SSHD $KIT_SSHD

# Ping Pong (health-check) - also use to check container version
ENV KIT_PHP_PING_PATH /ping-fpm
ENV KIT_PHP_PING_PONG "Pong ok - kit-php:${TAG}${KTAG}"

WORKDIR $KIT_APP_ROOT

COPY --chown=www-data:www-data ./src .
COPY ./config/templates ./tmp/config-tpl

# SSHD DEBUG
COPY ./config/ssh ./tmp/ssh
EXPOSE 22

VOLUME ["$KIT_APP_ROOT"]

COPY docker-kit-entrypoint /usr/local/bin/

RUN ["chmod", "+x", "/usr/local/bin/docker-kit-entrypoint"]

ENTRYPOINT ["docker-kit-entrypoint"]

# Override stop signal to stop process gracefully
# https://github.com/php/php-src/blob/17baa87faddc2550def3ae7314236826bc1b1398/sapi/fpm/php-fpm.8.in#L163
STOPSIGNAL SIGQUIT

EXPOSE $KIT_PHP_PORT

CMD ["php-fpm"]